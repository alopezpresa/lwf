#include "lwf_renderer.h"
#include "lwf_cocos2dx_resourcecache.h"

std::function<std::string(std::string const&)> LWFNS::TextRenderer::customGetFontPath = nullptr;

void LWFNS::TextRenderer::setCustomGetFontPathFunction(std::function<std::string(std::string const&)> f) {
	customGetFontPath = f;
}


std::string LWFNS::TextRenderer::getFontPath(std::string const& fontName, std::string const& fontExtension) {
	string fontPath = customGetFontPath ? customGetFontPath(fontName) : fontName;

	if (fontPath != fontName) {
		return fontPath;
	}

   	fontPath = cocos2d::LWFResourceCache::sharedLWFResourceCache()->getFontPathPrefix();
    fontPath += fontName;
	fontPath += fontExtension;

	return fontPath;
}
