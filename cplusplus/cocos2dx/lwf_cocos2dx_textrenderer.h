#ifndef LWF_COCOS2DX_TEXTRENDERER_H
#define LWF_COCOS2DX_TEXTRENDERER_H

#include "cocos2d.h"
#include "lwf_renderer.h"

namespace LWFNS {

class CocosTextRenderer : public TextRenderer
{
public:
	CocosTextRenderer(LWF *l) : TextRenderer(l) {}
	virtual ~CocosTextRenderer() {}

	virtual cocos2d::Label* GetLabel() = 0;
};

};

#endif
