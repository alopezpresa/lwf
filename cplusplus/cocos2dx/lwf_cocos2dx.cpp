#include "cocos2d.h"
#include "lwf_cocos2dx.h"
#include "lwf_cocos2dx_node.h"
#include "lwf_cocos2dx_bitmap.h"
#include "lwf_cocos2dx_textbmfont.h"
#include "lwf_cocos2dx_textttf.h"
#include "lwf_renderer.h"

namespace LWFNS {

	void fixContentSize(cocos2d::CCNode* node, float scale) {
		cocos2d::Size size = node->getContentSize();
		size.width *= scale;
		size.height *= scale;
		node->setContentSize(size);
	}

	void fixContentSize(vector<Object*> children, float scale) {
		static int fix = 0;
		for (auto it = children.begin(); it != children.end(); ++it) {
			Object* child = *it;
			if (child->IsMovie()) {
				fixContentSize(static_cast<Movie*>(child)->GetChildren(), scale);
			} else if (child->IsGraphic()) {
				fixContentSize(static_cast<Graphic*>(child)->GetChildren(), scale);
			} else if (child->IsBitmap()) {
				fixContentSize(static_cast<LWFBitmapRenderer*>(child->renderer.get())->GetSprite(), scale);
			}
		}
	}

	void setupLwf(shared_ptr<LWF>& lwf, int actions) {
		cocos2d::Director* director = cocos2d::Director::getInstance();

		if (actions & Actions::FIT_WIDTH) {
			lwf->FitForWidth(
				director->getVisibleSize().width,
				director->getVisibleSize().height,
				director->getOpenGLView()->getDesignResolutionSize().width,
				director->getOpenGLView()->getDesignResolutionSize().height,
				director->getOpenGLView()->getFrameSize().width,
				director->getOpenGLView()->getFrameSize().height
			);
		}

		if (actions & Actions::FIT_HEIGHT) {
			lwf->FitForHeight(
				director->getVisibleSize().width,
				director->getVisibleSize().height,
				director->getOpenGLView()->getDesignResolutionSize().width,
				director->getOpenGLView()->getDesignResolutionSize().height,
				director->getOpenGLView()->getFrameSize().width,
				director->getOpenGLView()->getFrameSize().height
			);
		}

		if (actions & Actions::SET_INTERACTIVE) {
			lwf->SetInteractive();
		}

		if (actions & Actions::RUN_EXEC) {
			lwf->Exec();
			fixContentSize(lwf->rootMovie->GetChildren(), lwf->scaleFactor);
		}
	}

	cocos2d::LWFNode* createLwfNode(char const* const url, int actions) {
		cocos2d::LWFNode* lwfNode = cocos2d::LWFNode::create(url);
		setupLwf(lwfNode->lwf, actions);
		return lwfNode;
	}
};

