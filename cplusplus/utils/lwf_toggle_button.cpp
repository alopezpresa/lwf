#include "lwf_toggle_button.h"
#include "lwf_cocos2dx.h"


namespace LWFNS {

ToggleButton::ToggleButton(
	Movie* root,
	char const* const buttonName,
	char const* const animationName,
	bool on
) : AnimatedButton(root, buttonName, animationName), on(on) {
	animation->GotoAndStop(getIdleAnimationName().c_str());
}

ToggleButton::ToggleButton() {}

ToggleButton::~ToggleButton() {
}

void ToggleButton::OnButtonTap(Button* button) {
	on = !on;
	AnimatedButton::OnButtonTap(button);
}

bool ToggleButton::isOn() {
	return on;
}

std::string ToggleButton::getIdleAnimationName() {
	return on ? "on" : "off";
}

std::string ToggleButton::getPressedAnimationName() {
	return on ? "onpressed" : "offpressed";
}

std::string ToggleButton::getDisabledAnimationName() {
	return on ? "ondisabled" : "offdisabled";
}

}
