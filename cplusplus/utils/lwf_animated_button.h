#ifndef LWF_ANIMATED_BUTTON_H
#define LWF_ANIMATED_BUTTON_H

#include <functional>
#include <string>

namespace LWFNS {

class Movie;
class Button;

typedef std::function<void(Button*)> ButtonEventHandler;

class AnimatedButton {
public:
	AnimatedButton(Movie* root, char const* const buttonName = "hitbox", char const* const animationName = "animation");
	AnimatedButton();
	virtual ~AnimatedButton();

	ButtonEventHandler OnPress;
	ButtonEventHandler OnRelease;
	ButtonEventHandler OnTap;
	ButtonEventHandler OnRollOut;

	void SetVisible(bool visible);
	void SetEnable(bool enable);

	void SetX(float x);
	void SetY(float y);
	float GetX() const;
	float GetY() const;

	Movie* GetMovie() const;
	Movie* GetAnimation() const;
	Button* GetButton() const;

protected:
	virtual void OnButtonPressed(Button*);
	virtual void OnButtonReleased(Button*);
	virtual void OnButtonRollOut(Button*);
	virtual void OnButtonTap(Button*);

	virtual std::string getIdleAnimationName();
	virtual std::string getPressedAnimationName();
	virtual std::string getDisabledAnimationName();

	Movie* animation;

private:
	void setCurrentNonPressedAnimation();
	Movie* movie;
	Button* button;

	int pressEventHandler;
	int releaseEventHandler;
	int rollOutEventHandler;
	bool tap;
};

}

#endif//LWF_ANIMATED_BUTTON_H
