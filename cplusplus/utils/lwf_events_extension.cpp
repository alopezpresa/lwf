#include "lwf_events_extension.h"
#include "lwf_core.h"
#include <functional>
#include <cstring>

int indexOf(char const* const source, char const* const pattern) {
	char const* const found = strstr(source, pattern);
	int result = (found - source);
	return (result >= 0) ? result : -1;
}

namespace LWFNS {

void ListenEvents(LWF* lwf, char const* const eventType, function<void(std::string const& eventName)> listener) {
	EventStrings const& eventNames = lwf->GetEventNames();
	size_t eventTypeSize = strlen(eventType);
	for (auto it = eventNames.begin(); it != eventNames.end(); ++it) {
		std::string eventName((*it).c_str());
		if (strncmp(eventType, eventName.c_str(), eventTypeSize) == 0 && (indexOf(eventName.c_str(), ":") == strlen(eventType))) {
			function<void(Movie*, Button*)> eventListener = std::bind(listener, eventName.substr(eventTypeSize + 1));
			lwf->AddEventHandler(eventName.c_str(), eventListener);
		}
	}
}

void ListenEvents(shared_ptr<LWF> lwf, char const* const eventType, function<void(std::string const& eventName)> listener) {
	ListenEvents(lwf.get(), eventType, listener);
}

}
